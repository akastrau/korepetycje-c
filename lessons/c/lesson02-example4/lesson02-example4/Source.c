#include <stdio.h>
#include <stdlib.h>

int main()
{
	int sum = 0;
	int n = 10;
	for (int i = 1; i <= n; i++)
	{
		sum += i;
	}
	printf("Sum: %i\n", sum);
	sum = n * (n + 1) / 2;
	printf("Sum: %i\n", sum);

	system("pause");
	return 0;
}