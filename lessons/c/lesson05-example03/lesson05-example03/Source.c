#include <stdio.h>
#include <stdlib.h>

typedef struct myStruct {
	int age;
	char* name;
	float mark;

} myStruct, *ptrMyStruct;

int main()
{
	myStruct data[10];
	ptrMyStruct anotherDataAsPtr = (myStruct*)malloc(sizeof(myStruct));

	// Do something cool :)

	free(anotherDataAsPtr);
	system("pause");
	return 0;
}