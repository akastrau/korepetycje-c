#include <stdio.h>
#include <stdlib.h>

int main() {
	FILE *file = NULL;
	errno_t error = fopen_s(&file, "file.txt", "w"); // otworz plik do zapisu (tryb tekstowy) wersja Windows

	printf("Tworzenie pliku... ");

	if (error != 0)
	{
		printf("blad!\nNie mozna utworzyc pliku!\n");
		return 1;
	}

	fprintf(file, "Jakis tekst");
	fclose(file);

	printf("ok\n");

	getchar();
	return 0;
}