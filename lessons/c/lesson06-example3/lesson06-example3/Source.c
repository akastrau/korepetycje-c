#include <stdio.h>
#include <stdlib.h>

int main() {
	FILE *fp = NULL;
	errno_t error = fopen_s(&fp, "data.txt", "r");

	if (error != 0)
	{
		printf("Blad!\n");
		getchar();
		return 1;
	}

	int fileSize = 0;
	fseek(fp, SEEK_SET, SEEK_END);
	fileSize = (int)ftell(fp);
	rewind(fp);

	char *buffer = (char*)calloc(fileSize, 1);

	if (buffer == NULL)
	{
		fclose(fp);
		printf("Blad alokacji pamieci!\n");
		return 1;
	}

	fread(buffer, 1, fileSize, fp);
	printf("Wyswietlam plik:\n\n");
	printf("%s\n", buffer);


	fclose(fp);
	free(buffer);

	getchar();
	return 0;
}