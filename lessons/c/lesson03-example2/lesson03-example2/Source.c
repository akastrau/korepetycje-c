#include <stdio.h>
#include <stdlib.h>

#define MASS 55 // Masa chytrej baby z Radomia (stala zdefiniowana)

float calculateEnergy(int speed, int howManyBottlesSheStoleYesterday)
{
	// Oblicz energie kinetyczna chytrej baby z Radomia
	int totalMass = MASS + (howManyBottlesSheStoleYesterday * 2);
	
	return (float)(totalMass * (speed * speed)) / 2; // castowanie (rzutowanie) typow
}

int main()
{
	float energy = calculateEnergy(5, 3); // 5 m/s2
	printf("Kinetic energy: %.2f\n", energy);

	system("pause");
	return EXIT_SUCCESS;
}