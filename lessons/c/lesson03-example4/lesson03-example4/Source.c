#include <stdlib.h>
#include <stdio.h>

void showMatrix(int matrix[][4], int nColumns, int nRows)
{
	printf("\t\tOur matrix:\n\n");
	for (size_t i = 0; i != nColumns; i++)
	{
		printf("\t\t");
		for (size_t j = 0; j != nRows; j++)
		{
			printf("%2i ", matrix[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

int main()
{
	int matrix[][4] =
	{
		{1,2,3,4},
		{5,6,7,8},
		{9,10,11,12}
	};

	int nColumns = sizeof(matrix) / sizeof(matrix[0]);
	int nRows = sizeof(matrix[0]) / sizeof(matrix[0][0]);

	showMatrix(matrix, nColumns, nRows);


	system("pause");
	return EXIT_SUCCESS;
}