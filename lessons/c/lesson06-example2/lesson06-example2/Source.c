#include <stdio.h>
#include <stdlib.h>

int main() {
	printf("Wyswietlanie pliku data.txt... ");

	FILE *file = NULL;
	errno_t error = fopen_s(&file, "data.txt", "r");

	if (error != 0)
	{
		printf("blad!\n");
		getchar();
		return 1;
	}

	char buffer[256] = { 0 };
	int counter = 1;

	printf("ok\n");

	while (fgets(buffer, sizeof(buffer), file) != NULL)
	{
		printf("Linia nr %i: %s", counter, buffer);
		counter++;
	}
	fclose(file);

	getchar();
	return 0;
}