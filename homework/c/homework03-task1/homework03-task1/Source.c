#include <stdio.h>
#include <stdlib.h>

#include "train.h"

int main()
{
	train* pendolino = (train*)malloc(sizeof(train)); // stworz nowy pociag
	pendolino->name = "Sobieski"; // nadaj mu nazwe
	pendolino->length = 0; // na razie brak przedzialow

	compartment* compart = (compartment*)malloc(sizeof(compartment)); // stworz nowy przedzial
	compart->number = 1; // nadaj mu numer
	compart->previous = NULL; // to jest poczatkowy przedzial, wiec nie ma poprzednika
	compart->next = NULL; // na razie drugiego przedzialu nie ma
	pendolino->compart = compart; // dodaj przedzial do pendolino

	compartment* compart2 = (compartment*)malloc(sizeof(compartment)); // stworz nowy przedzial
	compart2->previous = compart; // powiaz ten przedzial z poprzednim
	compart2->next = NULL; // to jest koniec - amba fatima i przedzialu ni ma
	compart2->number = 2; //nadaj mu numer

	compart->next = compart2; // powiedz, ze pierwszy przedzial ma sasiada

	// ZADANIE:
	// Sprobuj policzyc ile jest przedzialow i zmien dlugosc pociagu pendolino
	// Odpowiedz pendolino->length = 2; jest niedopuszczalna (czyli deklaracja wprost)

	free(compart2);
	free(compart);
	free(pendolino);


	system("pause");
	return 0;
}