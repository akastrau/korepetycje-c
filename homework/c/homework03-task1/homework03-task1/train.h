#pragma once

// Train structure
// If you want to extend the train, please reallocate memory
// 

typedef struct compartment {
	struct compartment *next;
	struct compartment *previous;
	int number;
} compartment;

typedef struct train {
	struct compartment *compart;
	int length; // How many compartments in the train
	char* name;
} train;
